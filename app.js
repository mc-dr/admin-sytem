//#region============================== Dependencies ==============================
var express = require("express");
var fileUpload = require('express-fileupload')
var serveIndex = require('serve-index')
var app = express();

var bodyparser = require("body-parser");
var session = require("express-session");
var nedbstore = require("nedb-session-store")(session);
var nedb = require("nedb");

const path = require('path');
app.set('template', path.join(__dirname,"template"));
app.set('view engine', 'pug');

app.locals.basedir = __dirname;

 //-___MysSQL-DB___ Connect
 var mysql =require('mysql');
 var con = mysql.createConnection
 ({
   host: "localhost",
   user: "Admin",
   password: "123Admin!@",
   database: "lizemaco_administration"
 });
 //#region______________________________ buildParams ______________________________
 function buildParams(req, additionalData) {
   var res = {...additionalData};
   if(req.session.loggedIn) {
     res.user = req.session.user;
     res.loggedIn = req.session.loggedIn
   }
   return res;
 }
 //#endregion

 con.connect(function(err) {
   if (err){
     throw err;
   }
   console.log("Connected to DB");
});
//#endregion
//#region============================== App Use ===================================
app.use('/ftp', express.static('uploads'), serveIndex('uploads', {'icons': true}));
app.use(fileUpload());
app.locals.basedir = __dirname;
app.use(bodyparser.urlencoded())
app.use(bodyparser.json())
app.use(express.static("assets"));
 app.use(
   session({
     name: "TheCookie",
     secret: "Randomjumble",
     resave: false,
     saveUnitialized: false,
     cookie:{path:"/", httpOnly: true, maxAge: 24*60*60*1000},
     store: new nedbstore({filename: "db/sessions.db"})
   })
 )


//#endregion
//#region============================== GET ===================================
app.get('/', function(req, res) {
  res.render('index',buildParams(req,{successStat:"", failureStat:""}));
})//Home/login page
app.get('/employees', function(req, res) {
  var sql_employees = "SELECT * FROM employees"; // fetch all users from SQL DB
  con.query(sql_employees, function(err,result)
    {
      res.render('employees',buildParams(req,{results:result, successStat:"", failureStat:""}))
    });
  })
app.get('/employeesDelete/:sa_id', function(req, res) {
  var sql_Pop_Edit = "SELECT * FROM employees WHERE sa_id = '" + req.params.sa_id+"'"; //fetch all data where ID = to the id sent from the employees page
  con.query(sql_Pop_Edit, function(err,result){
    if(err)throw err;
    res.render("employeesDelete.pug",buildParams(req,{results: result}));
  });
})
app.get('/employeesAdd', function(req, res) {

  res.render("employeesAdd.pug",buildParams(req));
})//Proto
app.get('/blocks', function(req, res) {
  var sql_employees = "SELECT * FROM employees";
  con.query(sql_employees, function(err,result)
    {
      res.send({results:result})
      //res.send(draw("views/employees.pug", buildParams(req, {results: result})));
    });
  })
app.get('/employees/:sa_id', function(req, res){
  var sql_Pop_Edit = "SELECT * FROM employees WHERE sa_id = '" + req.params.sa_id+"'";
  con.query(sql_Pop_Edit, function(err,result){
    res.render("employeesEdit.pug",buildParams(req, {results: result}));
  });
})//Proto
app.get('/upload', (req, res) => {
  res.render('upload',buildParams(req,{successStat:"", failureStat:""}));
})

app.get("/signout", function(req, res) {
  req.session.user = undefined;
  req.session.loggedIn = false;
  res.redirect('/');
})

app.listen(80, function() {
  console.log("App running.....");
  })
//#endregion
//#region============================== POST ===================================
app.post('/', function(req,res)
  {
    var idNum = ""
    idNum = req.body.UserID;
    var password = ""
    password =req.body.UserPass;
    var failure ="";
    var success ="";
    var user =idNum;
    var sql_SignIn_Username = "SELECT sa_id, password FROM employees WHERE sa_id = '"+idNum+"'";
    con.query(sql_SignIn_Username, function(err,result)
    {
      if(err) throw err;
       if (result=="")
        {
         failure = "ID or Password are in correct";
         res.render('index.pug',buildParams(req,{successStat:success, failureStat:failure}));
         return;
        }

        else
        {
          var idUI = result[0].sa_id;
          var passUI = result[0].password;
          console.log(idUI);
          console.log(passUI);
          if(idUI==idNum && passUI == password)
          {
            req.session.loggedIn = true;
            req.session.user = user;
            success = "logged in";
            res.redirect("/employees");
          }
          else if(idUI!==idNum || passUI !== password)
          {
            res.redirect('/');
            console.log("Dont match");
            return;
          }
          return;
        }
    });
    //console.log(req.session.loggedIn);
    //console.log(user)
  })
app.post('/employeesDelete/:sa_id', function(req,res)
{
 var sql_DeleteUser = "DELETE FROM employees WHERE sa_id='"+ req.params.sa_id+"'";
 con.query(sql_DeleteUser, function(err,result)
 {
   if(err) throw err;
   res.redirect('/employees')
 });
})
app.post('/employeesAdd', function(req,res)
{
  // Getting all data the user input and storing them in variables
  var id = req.body.saID
  var first = req.body.fName
  var last = req.body.lName
  var address = req.body.address
  var email = req.body.email
  var cell = req.body.cell
  var type = req.body.type
  var pass = req.body.password

  // SQl query to get data of all emplyees
  var sql_SelectAll = "SELECT * FROM employees";
  // SQl query to insert the users input into the DB
  var sql_InsertingUser = "INSERT INTO employees (sa_id, first_name, last_name, address, email, cell, type, password) VALUES('"+id+"', '"+first+"', '"+last+"', '"+address+"', '"+email+"', '"+cell+"', '"+type+"', '"+pass+"')";
  var only = true;
  var isNum = true;
  var failure ="";
  var success ="";
  con.query(sql_SelectAll, function(err,result1)
  {
    if(err) throw err;
    isNum = /^\d+$/.test(id); //Determine if user input is only numbers as it is an SA ID
    if(!isNum)
    {
      failure= "Invalid input";
    }
    // Determining if the user already exists in the DB
    for(i=0; i<result1.length; i++) // looping through all results
    {
      if(id == result1[i].sa_id)
      {
        only = false;
        failure = "User already exists";
        console.log(only);
        break;
      }
    }

    if(only&&isNum)
    {
      con.query(sql_InsertingUser, function(err,result2)
      {
        if(err) throw err;
        success="Insert was successful";
      });
    }
    con.query(sql_SelectAll, function(err,result)
    {
      res.render("employees.pug",buildParams(req, {results:result, successStat:success, failureStat:failure}));
    });
  });
})
app.post('/employees/:sa_id', function(req,res)
{
  var first = req.body.fName
  var last = req.body.lName
  var address = req.body.address
  var email = req.body.email
  var cell = req.body.cell
  var type = req.body.type
  var pass = req.body.password
  var only = true;
  var isNum = true;
  var failure ="";
  var success ="";

  // SQl query to get data of all emplyees
  var sql_SelectAll = "SELECT * FROM employees";
  var sql_UpdateUser = "UPDATE employees SET first_name='"+first+"', last_name='"+last+"', address= '"+address+"', email= '"+email+"', cell= '"+cell+ "', type='"+type+"',password='"+pass+"' WHERE sa_id= '"+ req.params.sa_id+"'";
  con.query(sql_UpdateUser, function(err, result2)
  {
    if(err) throw err;
    success="Update was successful";
  });
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ADD A FAILURAE CONDITION
  con.query(sql_SelectAll, function(err,result)
  {
    res.render("employees.pug", buildParams(req,{results:result, successStat:success, failureStat:failure}));
  });
})
//#region ----------------------- Upload --------------------------------------

app.post('/upload', function(req, res) {
  let sampleFile;
  let uploadPath;
  let failure ="";
  let success ="";
  //var directory = req.body.dir;

  if (!req.files || Object.keys(req.files).length === 0) {
    // res.status(400).send('No files were uploaded.');
    // return;
    failure = "No files were uploaded";
    res.render('upload.pug',buildParams(req,{successStat:success, failureStat:failure}));
  }

  console.log('req.files >>>', req.files); // eslint-disable-line

  sampleFile = req.files.sampleFile;

  uploadPath = __dirname + '/uploads/' + sampleFile.name;
  //mkdir directory
  //uploadPath = __dirname + '/uploads' + '/' + directory + '/' + sampleFile.name;

  sampleFile.mv(uploadPath, function(err) {
    if (err) {
      return res.status(500).send(err);
    }
    success = 'File uploaded to ' + uploadPath;
    res.render('upload.pug',buildParams(req,{successStat:success, failureStat:failure}));
    //res.send('File uploaded to ' + uploadPath);
  });
});

//#endregion

//#endregion
